# COMPX301-Assign1

First assignment (group) for COMPX301

A external sort merge written in rust, following the specifications of Solution A
from the [assignment instructions](https://www.cs.waikato.ac.nz/~tcs/COMPX301/assign1-2019.html).

We are using the Rust ascii string comparison operator for sorting input strings.
This should have a similar behaviour compared to Java's `String.compareTo()`, where
```
// Rust:
a > b == true
// When
// Java:
a.compareTo(b) > 0 == true
``` 

This program *will* sort blank lines.

# Authors
 - Daniel Martin - 1349779
 - Rex Pan - 1318909

# In This Project

 - **src/heap_sort_lib.rs** - A priority queue with a *stash* (can stash items to later be recovered).
 - **src/make_runs.rs** - Creates initial runs using the replacement selection strategy.
 - **src/poly_merge.rs** - Takes initial runs and sorts them using a polyphase merge sort.

# How To Use

To compile the binaries (`src/make_runs.rs` and `src/poly_merge.rs`) use cargo which will organise
dependencies and link them (`src/heap_sort_lib.rs`).

```
$cargo build --release --bin make_runs
$cargo build --release --bin poly_merge
```

The compiled binaries will be in `target/release/`

It is recommended to pipe the output of `make_runs` to `poly_merge`
as it passes the number of runs. `make_runs` takes one int `0 < n` which is the
maximum heap size (defaults to 10). `poly_merge` also takes one int `0 < n` which
is the number of temp files to use for the polyphase merge.
To sort `input.txt`, into `ouput.txt`, navigate to `target/release/` and run the binaries:

```
$cd target/release/
$cat input.txt | ./make_runs <max_heap_size> | ./poly_merge <num_of_temp_files> > output.txt
```

# Output

The output of the final binary, `poly_merge`, will be each input line in order.
Assuming the input is valid, there will also be 3 lines output to stderr, each
followed by their implied value:

 - `Input number of runs: `
 - `Calculated file writes: `
 - `Actual number of writes: `
