//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # heap_sort_lib
//!
//! Provides a leftist heap priority queue with a stash.
//!
//! The stash is a unordered array using the spare space at the end of the heap's vector.
//! The only way to fill the stack is via `replace_or_stash` and the only way to empty it
//! is via `flush_stash` (which pushes the items back onto the heap).


/// The leftest heap struct, contains the heap, heap size, max heap size and the comparision function
pub struct HeapPq<F, T>
where
    F: Fn(&T, &T) -> bool,
{
    memory: Vec<Option<T>>,
    current_heap_size: usize,
    max_heap_size: usize,
    comp: F,
}

impl<F, T> HeapPq<F, T>
where
    F: Fn(&T, &T) -> bool,
    T: Clone,
{
    /// Returns a new priority queue heap using the specified total memory and comparison function
    ///
    /// # Arguments
    ///
    /// * `total_memory` - The most items the priority queue should contain
    /// * `comp_func` - The function used for comparison where the first item rises to the top
    ///
    pub fn new(total_memory: usize, comp_func: F) -> Result<HeapPq<F, T>, ()> {
        if total_memory < 1 {
            return Err(());
        }

        Ok(HeapPq {
            // String::new() is very cheap, no need to create uninitialized memory
            memory: vec![None; total_memory],
            comp: comp_func,
            current_heap_size: 0,
            max_heap_size: total_memory,
        })
    }

    /// Pushes a new value onto the heap
    ///
    /// # Arguments
    ///
    /// * `new_value` - The value to put in the heap (takes ownership)
    ///
    pub fn push(&mut self, new_value: T) -> Result<(), ()> {
        self.push_raw(Some(new_value))
    }

    /// Returns a Option with a reference to the root item if it exists
    pub fn peek(&self) -> Option<&T> {
        if self.current_heap_size > 0 {
            Some(&self.memory.first().unwrap().as_ref().unwrap())
        } else {
            None
        }
    }

    /// Pops the first item off the heap
    pub fn pop(&mut self) -> Option<T> {
        if self.current_heap_size == 0 {
            return None;
        }
        let mut output = None;
        std::mem::swap(&mut self.memory[0], &mut output);
        self.memory.swap(0, self.current_heap_size - 1);

        self.current_heap_size -= 1;

        self.sink_down(0);

        output
    }

    /// Pop the top item and adds the new value
    /// Faster than a pop then push as it only need to balance the heap once.
    pub fn replace(&mut self, new_value: T) -> Option<T> {
        if self.current_heap_size == 0 {
            return None;
        }
        let mut output = Some(new_value);
        std::mem::swap(&mut self.memory[0], &mut output);

        self.sink_down(0);

        output
    }

    /// Returns the top item and either pushes or stashes the new value
    ///
    /// If the new value should be higher than the root of the stack, then stash the new value.
    ///
    /// # Arguments
    ///
    /// * `new_value` - The value to put in the heap (takes ownership)
    ///
    pub fn replace_or_stash(&mut self, new_value: T) -> Result<T, ()> {
        if self.current_heap_size == 0 {
            return Err(());
        }

        if (self.comp)(&new_value, &self.peek().unwrap()) {
            // Stash
            let data = self.pop();
            self.memory[self.current_heap_size] = Some(new_value);
            self.max_heap_size -= 1;
            Ok(data.unwrap())
        } else {
            // Replace
            let mut output = Some(new_value);
            std::mem::swap(&mut self.memory[0], &mut output);
            self.sink_down(0);
            Ok(output.unwrap())
        }
    }

    /// Returns any stash items to the heap
    pub fn flush_stash(&mut self) {
        while self.max_heap_size != self.memory.len() {
            let mut temp = None;
            std::mem::swap(&mut temp, &mut self.memory[self.max_heap_size]);
            self.max_heap_size += 1;
            self.push_raw(temp).expect("Max heap size smaller current size?");
        }
    }

    /// Returns the size of the heap (not counting the stash)
    pub fn current_heap_size(&self) -> usize {
        self.current_heap_size
    }

    /// Returns `true` if the heap is full
    /// (And therefore no stack can exist)
    pub fn full(&self) -> bool {
        self.max_heap_size == self.current_heap_size
    }

    /// Returns `true` if the heap is empty (not counting the stash)
    pub fn empty(&self) -> bool {
        self.peek().is_none()
    }

    /// Returns the parent index of a given index
    #[inline]
    fn parent(idx: usize) -> usize {
        (idx - 1) / 2
    }

    /// Returns the left child index of a given index
    /// *The right child will be +1 if it exists*
    #[inline]
    fn left_child(idx: usize) -> usize {
        idx * 2 + 1
    }

    /// Push a value contained in a Option (how items are stored in the heap)
    fn push_raw(&mut self, new_value: Option<T>) -> Result<(), ()> {
        if self.current_heap_size == self.max_heap_size {
            return Err(());
        }

        let mut idx = self.current_heap_size;
        self.current_heap_size += 1;
        self.memory[idx] = new_value;

        while idx != 0
            && !(self.comp)(
                &self.memory[Self::parent(idx)].as_ref().unwrap(),
                &self.memory[idx].as_ref().unwrap(),
            )
        {
            self.memory.swap(Self::parent(idx), idx);
            idx = Self::parent(idx);
        }

        Ok(())
    }

    /// Sinks the value at the specified index until it is in order
    fn sink_down(&mut self, mut idx: usize) {
        while idx < self.current_heap_size / 2 {
            let mut child_idx = Self::left_child(idx); // left child

            // does right child exist and is whatever the comp fn defines than the left child
            if child_idx + 1 < self.current_heap_size
                && !(self.comp)(
                    &self.memory[child_idx].as_ref().unwrap(),
                    &self.memory[child_idx + 1].as_ref().unwrap(),
                )
            {
                child_idx = child_idx + 1; // right child
            }

            // parent is (comp fn) than children (in order, done)
            if !(self.comp)(&self.memory[child_idx].as_ref().unwrap(), &self.memory[idx].as_ref().unwrap()) {
                break;
            }

            self.memory.swap(child_idx, idx);
            idx = child_idx;
        }
    }
}

#[cfg(test)]
mod heap_tests {
    use super::HeapPq;

    /// This test tests for a MaxHeap, flip comp when you need aa min heap
    #[test]
    fn check_pq_works() {
        let mut pq = HeapPq::new(10, |a, b| a > b).unwrap();
        assert_eq!(pq.current_heap_size, 0);
        assert_eq!(pq.peek(), None);

        // check insert and peek
        pq.push("Kharbranth".to_owned()).unwrap();
        assert_eq!(pq.peek(), Some(&"Kharbranth".to_owned()));
        pq.push("Aqasix".to_owned()).unwrap();
        assert_eq!(pq.peek(), Some(&"Kharbranth".to_owned()));
        pq.push("Yaezir".to_owned()).unwrap();
        assert_eq!(pq.peek(), Some(&"Yaezir".to_owned()));
        pq.push("zbcd".to_owned()).unwrap();
        assert_eq!(pq.peek(), Some(&"zbcd".to_owned()));
        pq.push("Zahel".to_owned()).unwrap();
        assert_eq!(pq.peek(), Some(&"zbcd".to_owned()));

        // Check pop
        assert_eq!(pq.pop(), Some("zbcd".to_owned()));
        assert_eq!(pq.peek(), Some(&"Zahel".to_owned()));

        // Check replace
        assert_eq!(pq.replace("bob".to_owned()), Some("Zahel".to_owned()));
        assert_eq!(pq.replace("Bill".to_owned()), Some("bob".to_owned()));
        assert_eq!(pq.pop(), Some("Yaezir".to_owned()));

        assert_eq!(pq.peek(), Some(&"Kharbranth".to_owned()));

        pq.push("Sunraiser".to_owned()).unwrap();

        // Check remaining list is in order
        assert_eq!(pq.pop(), Some("Sunraiser".to_owned()));
        assert_eq!(pq.pop(), Some("Kharbranth".to_owned()));
        assert_eq!(pq.pop(), Some("Bill".to_owned()));
        assert_eq!(pq.pop(), Some("Aqasix".to_owned()));
        assert_eq!(pq.pop(), None);
    }

     #[test]
     fn check_pq_returns_error_on_zero() {
         assert!(HeapPq::<_, String>::new(0, |a, b| a < b).is_err());
     }

    #[test]
    fn check_pq_returns_err_on_full_insert() {
        let mut pq = HeapPq::new(3, |a, b| a < b).unwrap();
        pq.push("abc".to_owned()).expect("Pq Full Too Early");
        pq.push("abc".to_owned()).expect("Pq Full Too Early");
        pq.push("abc".to_owned()).expect("Pq Full Too Early");
        assert!(pq.push("abc".to_owned()).is_err());
    }
}
