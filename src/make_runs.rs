//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # make_runs
//!
//! This file takes everything in stdin, sorts chunks of it and dumps to `test_out/init_runs.txt`.
//! Runs are sorted using the replacement selection strategy.
//! The number of runs created is printed to `stdout`.
//!
//! # Input
//!
//! * A file of new line separated strings (via stdin)
//! * An integer specifying the size of the buffer used to sort data (via arg, defaults to 10)
//!
//! # Example
//!
//! Create runs for input_file.txt using a heap size of 20
//!
//! ```
//! $cat input_file.txt | make_runs 20
//! ```

mod heap_sort_lib;

use heap_sort_lib::HeapPq;
use std::env;
use std::fs::OpenOptions;
use std::io::{BufRead, BufReader, BufWriter, Write};

fn main() {
    let mem_size = if let Some(arg1) = env::args().nth(1) { arg1.parse::<usize>().unwrap() } else { 10 };

    let reader = BufReader::new(std::io::stdin());

    let lines = reader.lines().map(|c| c.expect("need proper unicode"));

    std::fs::create_dir_all("test_out/").expect("Failed to create test files dir");

    let mut writer = BufWriter::new(
        OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open("test_out/init_runs.txt")
            .expect("File Failed"),
    );

    // Create the priority queue with the input memory size and comparison function
    let mut pq = HeapPq::new(mem_size, |a, b| a < b).expect("Memory size must be at least 1");

    let mut run_count: u64 = 0;

    for line in lines {
        if !pq.full() {
            // At the start if a run, push lines into pq till it's full
            pq.push(line)
                .expect("Failed top push into Pq, already max capacity, full() not registering correctly?");
        } else if !pq.empty() {
            // While there are still things in the pq, (i.e. not all memory is stashed)

            let mut out = pq.replace_or_stash(line).unwrap();
            // Add a placeholder if the line is empty (also if placeholder already exists)
            out = if out.is_empty() || out.ends_with("-") { out + "-" } else { out };

            writer.write((out + "\n").as_bytes()).expect("Failed to write to file");
        } else {
            // Run finished, the pq's memory is completely used up by the stash

            pq.flush_stash();
            let mut out = pq.replace_or_stash(line).unwrap();
            out = if out.is_empty() || out.ends_with("-") { out + "-" } else { out };

            run_count += 1;
            // End run by adding a new line, then output the first item of the next run
            writer.write(format!("\n{}\n", out).as_bytes()).expect("Failed to write to file");
        }
    }

    // TODO: We don't need this copy to see head change
    let old_size = pq.current_heap_size();
    pq.flush_stash();

    if old_size != pq.current_heap_size() {
        // Size has changed! (so there must be a new head in the stash) so start a new run
        writer.write(b"\n").expect("Failed to write to file");
        run_count += 1;
    }

    // Empty out the remaining heap
    if !pq.empty() {
        run_count += 1
    }

    while !pq.empty() {
        let mut out = pq.pop().unwrap();
        out = if out.ends_with("-") { out + "-" } else { out };
        writer.write((out + "\n").as_bytes()).expect("Failed to write to file");
    }

    // Print out how many runs were made, so poly_merge knows
    println!("{}", run_count);
}
