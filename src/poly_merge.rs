//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # poly_merge
//!
//! `poly_merge` takes `test_out/init_runs.txt`, the number of runs it contains via stdin
//! and one argument for how many files to split into. It produces all the data sorted
//! line by line via stdout. The number of file reads besides the initial read of
//! `init_runs.txt`, is output in stderr.
//!
//! There are three main functions for our polyphase merge sort:
//!  - **calculate_run_split** - Calculates the number of runs to put in each file,
//! guaranteed to have a perfect merge by using virtual/empty runs (ends on one run).
//!  - **split_for_merge** - Creates the temp files and distributes runs width first
//! (virtual runs will always be last in a file)
//!  - **merge** - Merges one run from the top of each file into the output file, then
//! repeats until a file is empty. Once a file is empty, the input becomes the input file
//! and the empty output file becomes the input file. This repeats until the last run
//! where it will output to stdout rather than the output file.
//!
//! # Input
//!
//! * `test_out/init_runs.txt` containing sorted runs
//! * An integer specifying the number of files to use (via arg, defaults to 3)
//! * An integer specifying the runs in `test_out/init_runs.txt` (via stdin)
//!
//! # Output
//!
//! Returns the ordered data line by line in stdout.
//! Assuming the input is valid, there will also be 3 lines output to stderr, each
//! followed by their implied value:
//!
//! * `Input number of runs: `
//! * `Calculated file writes: `
//! * `Actual number of writes: `
//!
//! # Example
//!
//! Order `test_out/init_runs.txt` with 10 runs using 5 files, output to `output.txt`
//!
//! ```
//! $echo 10 | poly_merge 5 > output.txt
//! ```

mod heap_sort_lib;

use heap_sort_lib::HeapPq;
use std::env;
use std::fs::{File, OpenOptions};
use std::io::BufReader;
use std::io::{BufRead, BufWriter, Write};

const ACCEPTABLE_EXTRA_RUNS_RATIO: f32 = 0.5;
const INPUT_PATH: &str = "test_out/init_runs.txt";
const TEMP_PATH_PREFIX: &str = "test_out/poly_merge_temp_";

fn main() {
    let mut write_open_count: u32 = 0;

    // Obtain number of files to create
    let num_of_files: u32 = if let Some(arg1) = env::args().nth(1) {
        arg1.parse().expect("First argument not a number (number of files)")
    } else {
        3
    };

    if num_of_files < 3 {
        panic!("Number of files must be at least 3 (2 for merge; one for output)");
    }

    let mut num_of_runs = String::new();
    std::io::stdin().read_line(&mut num_of_runs).expect("Failed to read from stdin");

    let num_of_runs: u32 = num_of_runs.trim().parse().expect("First stdin line not a valid runs number"); // could be nan or negative

    eprintln!("Input number of runs: {}", num_of_runs);

    if num_of_runs > 0 {
        // sort runs into n files
        let (split, total_runs) = calculate_run_split(num_of_files, num_of_runs);

        // create files from init_runs.txt
        split_for_merge(split);
        // reader closed

        // do merge
        merge(num_of_files, &mut write_open_count, total_runs);
    }

    eprintln!("Actual number of writes: {}", write_open_count); // does not count writes when splitting initial runs to temp files
}

#[derive(PartialEq)]
enum FileState {
    Content,
    End,
    EndOfFile,
}

struct FileWriter {
    stream: Box<Write>,
    file_num: u32,
}

struct FileReader<I: Iterator<Item=Result<String, std::io::Error>>> {
    stream: I,
    file_num: u32,
    file_state: FileState,
}

fn split_for_merge(split_vec: Vec<u32>) {
    // create reader for input file
    let mut reader =
        BufReader::new(File::open(INPUT_PATH).expect("File can't be opened")).lines().filter_map(|c| c.ok());

    // Create writer streams (and the temp files)
    let mut split_file_streams = Vec::with_capacity(split_vec.len());
    for file_num in 0..split_vec.len() {
        // pass dummy counter to get_writer as we are just making initial files
        let writer = get_writer(file_num as u32, &mut 0);
        if split_vec[file_num] != 0 {
            split_file_streams.push((writer, split_vec[file_num]));
        }
    }

    let mut full_files = 0;
    while full_files < split_file_streams.len() {
        // While still filling files
        for file in 0..split_file_streams.len() {
            // For each file
            if split_file_streams[file].1 == 0 {
                continue;
            } // Skip file if full
            loop {
                // loop for a "run"
                let new_val = match reader.next() {
                    Some(val) => val,
                    None => "".to_owned(),
                };

                let empty_line = new_val.is_empty();
                // Not last new line
                if split_file_streams[file].1 != 1 || !empty_line {
                    split_file_streams[file]
                        .0
                        .write(format!("{}\n", new_val).as_bytes())
                        .expect("Failed to write to file");
                }

                // hit end of a run
                if empty_line {
                    split_file_streams[file].1 -= 1;
                    if split_file_streams[file].1 == 0 {
                        full_files += 1;
                    }
                    break;
                }
            }
        }
    }
}

fn merge(n_way: u32, write_open_count: &mut u32, mut number_of_runs: u32) {
    // The heap will contain at most one value from each file,
    // To know which file to read when taking a value, we must
    // also store which file the value came from. (value, file_num)
    let mut pq = HeapPq::new(n_way as usize, |a: &(String, usize), b: &(String, usize)| *a.0 < *b.0).unwrap();

    // Put all readers into a array with there file number and state
    let mut input_files_data = Vec::with_capacity((n_way - 1) as usize);
    for file_num in 0..(n_way - 1) {
        input_files_data.push(FileReader {
            stream: get_reader(file_num).lines(),
            file_num,
            file_state: FileState::Content,
        });
    }

    let mut last_run = false;

    // Create writer for output, although not likely at
    // the start, if runs to go are smaller than n_way,
    // we should go straight to stdout rather than a temp file
    let mut output_file_data = if number_of_runs < n_way {
        last_run = true;
        FileWriter {
            stream: Box::new(std::io::stdout()),
            file_num: 0,
        }
    } else {
        FileWriter {
            stream: Box::new(get_writer(n_way - 1, write_open_count)),
            file_num: n_way - 1,
        }
    };

    // 1 loop = 1 run in output
    loop {
        // Option to contain the file_num of the last file to empty after a run
        let mut hit_end: Option<usize> = None;

        // fill up pq
        for file in 0..input_files_data.len() {
            if input_files_data[file].file_state == FileState::End {
                // new run, set active file stream to content (End is just end of run, not file)
                input_files_data[file].file_state = FileState::Content;
            } else if input_files_data[file].file_state == FileState::EndOfFile {
                continue; // don't count an ended file more than once
            }

            // get the next line in the file
            match input_files_data[file].stream.next() {
                Some(Ok(mut val)) => {
                    if !val.is_empty() {
                        if val.ends_with("-") {
                            val.pop();
                        }
                        pq.push((val, file)).unwrap();
                    } else {
                        // end of run but still more in file
                        input_files_data[file].file_state = FileState::End;
                        number_of_runs -= 1;
                    }
                }
                None => {
                    // end of run and file
                    input_files_data[file].file_state = FileState::EndOfFile;
                    hit_end = Some(file);
                    number_of_runs -= 1;
                },
                Some(Err(err)) => panic!("Error while reading stream: {:?}", err)
            }
        }

        // Output smallest value from pq and replace (while still more lines in that file's run)
        // Until all runs from each input file has been ouput (pq empty)
        while !pq.empty() {
            let top_file: usize = pq.peek().expect("pq empty but pq.empty==true").1;

            // Get the next item on the heap, and replace with the next value
            // if there is more in that file's run.
            let new_val = if input_files_data[top_file].file_state != FileState::Content {
                pq.pop().unwrap().0
            } else {
                match input_files_data[top_file].stream.next() {
                    Some(Ok(mut val)) => {
                        if !val.is_empty() {
                            // read a normal value, replace top value
                            // if ends with "-" remove it (for empty lines)
                            if val.ends_with("-") {
                                val.pop();
                            }
                            pq.replace((val, top_file)).unwrap().0
                        } else {
                            // read an end of run line
                            // (only happens once a run as the ref is popped from pq)
                            number_of_runs -= 1;
                            pq.pop().unwrap().0
                        }
                    }
                    None => {
                        // end of file
                        input_files_data[top_file].file_state = FileState::EndOfFile;
                        hit_end = Some(top_file);
                        number_of_runs -= 1;
                        pq.pop().unwrap().0
                    },
                    Some(Err(err)) => panic!("Error while reading stream: {:?}", err)
                }
            };

            // Add a "-" on empty lines (and lines that end with a "-") to differentiate
            // a end of run from a empty line
            let new_val = if (new_val.is_empty() || new_val.ends_with("-")) && !last_run {
                new_val + "-"
            } else {
                new_val
            };

            output_file_data
                .stream
                .write((format!("{}\n", new_val)).as_bytes())
                .expect("Failed to write to file");
        }
        number_of_runs += 1; // just made one run in output

        if number_of_runs == 1 {
            // All input files ended, completely finished assuming split calc was correct
            break;
        }

        // swap input and empty output file if a file is empty
        match hit_end {
            Some(file) => {
                // save output file number before overwritten
                let file_num = output_file_data.file_num;

                // Output to std::out if on last run
                output_file_data = if number_of_runs < n_way {
                    last_run = true;
                    FileWriter { stream: Box::new(std::io::stdout()), file_num: 0 }
                } else {
                    FileWriter {
                        stream: Box::new(get_writer(input_files_data[file].file_num, write_open_count)),
                        file_num: input_files_data[file].file_num,
                    }
                };

                // Input to old output file
                let mut temp_data = FileReader {
                    stream: get_reader(file_num).lines(),
                    file_num,
                    file_state: FileState::Content,
                };

                std::mem::swap(&mut input_files_data[file], &mut temp_data);
            }
            _ => {
                // no file ended continue to next run, same files
                output_file_data.stream.write("\n".as_bytes()).expect("Failed to write to file");
            }
        }
    }
}

fn get_writer(num: u32, write_open_count: &mut u32) -> BufWriter<File> {
    *write_open_count += 1;
    BufWriter::new(
        OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(format!("{}{}.txt", TEMP_PATH_PREFIX, num))
            .expect("File Failed"),
    )
}

fn get_reader(num: u32) -> BufReader<File> {
    BufReader::new(File::open(format!("{}{}.txt", TEMP_PATH_PREFIX, num)).expect("File can't be opened"))
}

fn calculate_run_split(n_way: u32, number_of_runs: u32) -> (Vec<u32>, u32) {

    if n_way <= 2 {
        return (Vec::new(), 0);
    }

    let mut current_count = 1;
    let mut col = vec![0; n_way as usize];
    if number_of_runs == 0 {
        return (col, 0);
    }
    let number_of_runs = number_of_runs.max(n_way + 1);

    col[0] = 1;

    // First simulate a run backwards where it chooses the biggest run to add.

    let mut temp_vec = Vec::new();
    let mut temp_count = 0;
    while current_count < number_of_runs {
        temp_count = current_count;
        temp_vec = col.clone();
        next_iteration(&mut col, &mut current_count, true);
    }

    // Then if there are too many virtual runs, step back and forward again but choosing the smallest instead
    // Repeat until within acceptable virtual run count.
    while current_count - number_of_runs > (number_of_runs as f32 * ACCEPTABLE_EXTRA_RUNS_RATIO) as u32 {
        current_count = temp_count;
        col = temp_vec.clone();
        while current_count < number_of_runs {
            next_iteration(&mut col, &mut current_count, false);
        }
        prev_iteration(&mut temp_vec, &mut temp_count);
    }

    // Swap zero with the last element, so the last element is always zero when outputted.
    let zero_idx = col.iter().enumerate().find(|&(_, c)| *c == 0).unwrap().0;
    col.swap(zero_idx, n_way as usize - 1);

    let mut iter_count = 0;

    // Just run it back to the start to count number of iterations, and test prev_iteration is correct.
    // Not that performance heavy, since there are never that many phases.
    temp_count = current_count.clone();
    temp_vec = col.clone();
    while temp_count != 1 {
        prev_iteration(&mut temp_vec, &mut temp_count);
        iter_count += 1;
    }
    eprintln!("Calculated file writes: {}", iter_count - 1);
    (col, current_count)
}

/// Moves back a phase in the polyphase sort merge
/// is_max specifies if we are choosing the max run count to add to all files, otherwise the minimum is used
fn next_iteration(iters: &mut [u32], current_count: &mut u32, is_max: bool) {
    let (nxt_idx, &nxt_val) = if is_max {
        iters.iter().enumerate().max_by(|&(_, a), &(_, b)| a.cmp(b)).unwrap()
    } else {
        iters.iter().enumerate().min_by_key(|&(_, a)| if *a == 0 { std::u32::MAX } else { *a }).unwrap()
    };

    *current_count += nxt_val * (iters.len() as u32 - 2);

    iters.iter_mut().enumerate().for_each(|(idx, c)| if idx != nxt_idx { *c += nxt_val } else { *c = 0 });
}

/// Moves forward a phase in the polyphase sort merge
fn prev_iteration(iters: &mut [u32], current_count: &mut u32) {
    let (prev_idx, &prev_val) =
        iters.iter().enumerate().min_by_key(|&(_, a)| if *a == 0 { std::u32::MAX } else { *a }).unwrap();

    *current_count -= prev_val * (iters.len() as u32 - 2);

    iters.iter_mut().enumerate().for_each(|(idx, c)| {
        if idx == prev_idx {
            *c = 0
        } else if *c == 0 {
            *c = prev_val
        } else {
            *c -= prev_val
        }
    });
}
